<?php

$sponsors = array(
    array('nombre' => 'Marinas Alto Manzano', 'url' => 'http://www.marinasaltomanzano.com.ar', 'logo' => 'marinas_2.png', 'contacto' => 'Marce'),
    array('nombre' => 'Uno Bahía Club', 'url' => 'http://www.unobahiaclub.com.ar', 'logo' => 'uno.png', 'contacto' => 'Marce'),
    array('nombre' => 'El Bosque Chocolate', 'url' => '', 'logo' => 'bosquechocolate.png', 'contacto' => 'Marce'),
    array('nombre' => 'Bayo Abajo', 'url' => 'https://century21.com.ar/inmobiliaria/25-century-21-caballero-villa-la-angostura-neuquen-argentina', 'logo' => 'century21.png', 'contacto' => 'Pablo'),

    array('nombre' => 'Bayo Abajo', 'url' => '', 'logo' => 'bayo-abajo.png', 'contacto' => 'Marce'),
    array('nombre' => 'Genesis', 'url' => 'https://www.facebook.com/pretorian.gym', 'logo' => 'genesis.png', 'contacto' => 'Marce'),
    array('nombre' => 'AquaShop', 'url' => 'https://www.facebook.com/Aquashopatagonia/', 'logo' => 'aquashop.png', 'contacto' => 'Andres'),
    array('nombre' => 'cronometrajeinstantaneo.com', 'url' => 'http://cronometrajeinstantaneo.com', 'logo' => 'cronometrajeinstantaneo.png', 'contacto' => 'Andy'),

    array('nombre' => 'Del Caballero', 'url' => 'https://www.delcaballerohotel.com.ar', 'logo' => 'castillo.png', 'contacto' => 'Marce'),
    array('nombre' => 'Municipalidad de Villa la Angostura', 'url' => 'http://villalaangostura.gov.ar', 'logo' => 'municipalidad.png', 'contacto' => 'Marce'),

);

$colaboradores = array(
    array('nombre' => 'AK Arquitectura', 'url' => 'http://www.akquitectura.com.ar', 'logo' => 'akarquitectura.png', 'contacto' => 'Pali'),
    array('nombre' => 'Buceo Patagonia', 'url' => '', 'logo' => 'buceo.png', 'contacto' => ''),
    array('nombre' => 'GB Consultores', 'url' => 'http://www.gbconsultores.com.ar', 'logo' => 'gbconsultores_transparent.png', 'contacto' => 'Pali'),
    array('nombre' => 'Bomberos Voluntarios de Villa la Angostura', 'url' => 'https://www.facebook.com/bomberos.vla', 'logo' => 'bomberos_transparent.png', 'contacto' => 'Andrés'),
    array('nombre' => 'Il Preto Gelato', 'url' => '', 'logo' => 'ilpreto.png', 'contacto' => 'Marce'),
    array('nombre' => 'Alquiler de moto', 'url' => '', 'logo' => 'motorent.png', 'contacto' => 'Marce'),

    array('nombre' => 'Doschi', 'url' => '', 'logo' => 'doschi.png', 'contacto' => 'Pali'),
    array('nombre' => 'Prefectura Naval Argentina', 'url' => 'http://www.prefecturanaval.gov.ar', 'logo' => 'prefectura.svg', 'contacto' => 'Marce'),
    array('nombre' => 'Administración de Parques Nacionales', 'url' => 'http://www.parquesnacionales.gov.ar', 'logo' => 'parques.png', 'contacto' => 'Marce'),
    array('nombre' => 'La Segunda', 'url' => 'http://www.lasegunda.com.ar', 'logo' => 'lasegunda.png', 'contacto' => 'Marce'),
    array('nombre' => 'Diario Andino', 'url' => 'http://www.diarioandino.com', 'logo' => 'diarioandino.png', 'contacto' => 'Gonza'),
    array('nombre' => 'Fusión Gráfica', 'url' => 'http://www.fusionpatagonia.com.ar', 'logo' => 'fusion.png', 'contacto' => 'Marce'),
    array('nombre' => 'Dry Wall', 'url' => '', 'logo' => 'drywall.jpg', 'contacto' => 'Marce'),

	);

// ANÓNIMOS
	// array('nombre' => 'Bahía Manzano', 'url' => 'http://www.bahiamanzano.com', 'logo' => 'bahiamanzano.png', 'contacto' => 'Pali'),

// VIEJOS
    // array('nombre' => 'Sailfish', 'url' => '', 'logo' => 'sailfish.png', 'contacto' => 'Marce'),
    // array('nombre' => 'Daniel Alonso', 'url' => 'http://www.arquitectodanielalonso.com', 'logo' => 'danielalonso.jpg', 'contacto' => 'Dani'),
    // array('nombre' => 'Destino VLA', 'url' => 'http://www.destinovla.com', 'logo' => 'destinovla.png', 'contacto' => 'Marce'),
	// array('nombre' => 'Nodo Mix', 'url' => 'http://www.zonanodo.com.ar', 'logo' => 'nodomix.png', 'contacto' => 'Andy'),
	// array('nombre' => 'Corralón Arenas', 'url' => 'http://www.zonanodo.com.ar', 'logo' => 'arenas.jpg', 'contacto' => 'Andy'),
	// array('nombre' => 'Compured', 'url' => 'http://www.netpatagon.net', 'logo' => 'compured.jpg', 'contacto' => 'Gonza'),
	// array('nombre' => 'Sol Arrayan - Hotel & Spa', 'url' => 'http://www.solarrayan.com', 'logo' => 'sol_arrayan.jpg', 'contacto' => 'Pali'),
	// array('nombre' => 'Consultorios San Camilo', 'url' => '', 'logo' => '', 'contacto' => 'Mariana'),
	// array('nombre' => 'Fernandez Barrios Propiedades', 'url' => '', 'logo' => 'fernandezbarrios.png', 'contacto' => 'Marce'),
	// array('nombre' => 'Enviatur', 'url' => '', 'logo' => '', 'contacto' => 'Pablo'),
    // array('nombre' => 'Viejos Tiempos', 'url' => 'https://www.facebook.com/viejostiempos.restaurante', 'logo' => 'viejostiempos.png', 'contacto' => 'Pablo'),
    // array('nombre' => 'KMI', 'url' => 'http://www.kminatacion.com', 'logo' => 'kmi.png', 'contacto' => ''),
    // array('nombre' => 'Josecito', 'url' => '', 'logo' => 'josecito.png', 'contacto' => ''),
    // array('nombre' => 'Estudio a2 Store', 'url' => 'http://www.facebook.com/estudioa2store', 'logo' => 'a2.jpg', 'contacto' => 'Javes'),

    // array('nombre' => 'Mamuschka', 'url' => '', 'logo' => 'mamuschka.jpg', 'contacto' => 'Marce'),
    // array('nombre' => 'DAK', 'url' => 'http://www.almacendenatacion.com.ar', 'logo' => 'dak.webp', 'contacto' => 'Andres'),
	// array('nombre' => 'El Club de Beneficios VLA', 'url' => '', 'logo' => '', 'contacto' => ''),
	// array('nombre' => 'Colegio de arquitectos', 'url' => '', 'logo' => '', 'contacto' => 'Pali'),

