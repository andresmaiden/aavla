<?php
$url = filter_input(INPUT_GET, 'url', FILTER_SANITIZE_URL);

if (!$url || !file_exists('textos/'.$url.'.html'))
    $url = 'inicio';

include 'datos.php';

?>
<!DOCTYPE HTML>
<!--
    ZeroFour 2.5 by HTML5 UP
    html5up.net | @n33co
    Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>Aguas Abiertas Villa la Angostura</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800" rel="stylesheet" type="text/css" /> -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.dropotron.min.js"></script>
        <script src="js/config.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-panels.min.js"></script>
        <noscript>
            <link rel="stylesheet" href="css/skel-noscript.css" />
            <link rel="stylesheet" href="css/style.css" />
            <link rel="stylesheet" href="css/style-desktop.css" />
        </noscript>
        <!--[if lte IE 9]><link rel="stylesheet" href="css/ie9.css" /><![endif]-->
        <!--[if lte IE 8]><script src="js/html5shiv.js"></script><link rel="stylesheet" href="css/ie8.css" /><![endif]-->
        <!--[if lte IE 7]><link rel="stylesheet" href="css/ie7.css" /><![endif]-->
        <script type="text/javascript">
        // $(function() {
        //  $("a").on("click", function(e) {
        //      if ($(this).attr("target") != '_blank') {
        //          e.preventDefault();
        //          $.ajax({
        //              url: 'textos/'+$(this).attr("href")+'.html',
        //              type: 'get',
        //              beforeSend: function() {
        //              },
        //              success: function(data) {
        //                  $("#presentacion").hide();
        //                  $("#para_rellenar"  ).html(data);
        //                  var offset = $("#para_rellenar").offset();
  //                            $("html, body").animate({ scrollTop: offset.top }, 1000);
        //              },
        //              error: function() {
        //                  alert("Se ha producido un error comunicándose con el servidor");
        //              }
        //          });
        //      }
        //  });
        // });
        </script>

        <style type="text/css">
            .responsive-video {
                position: relative;
                padding-bottom: 56.25%;
                padding-top: 60px; overflow: hidden;
            }

            .responsive-video iframe,
            .responsive-video object,
            .responsive-video embed {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }
        </style>
    </head>
    <body class="homepage">

        <!-- Header Wrapper -->
            <div id="header-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="12u">

                            <!-- Header -->
                                <header id="header">
                                    <div class="inner">

                                        <!-- Logo -->
                                            <h1><a href="https://aavla.com.ar" id="logo">Aguas Abiertas Villa la Angostura</a></h1>

                                        <!-- Nav -->
                                            <nav id="nav">
                                                <ul>
                                                    <li>
                                                        <span>¿Qué estás buscas?</span>
                                                        <ul>
                                                            <li><a href="inicio">Inicio</a></li>
                                                            <li><a href="https://www.cronometrajeinstantaneo.com/resultados/aavla-2022/generales" target="_blank">Resultados de AAVLA 2022</a></li>
                                                            <li><a href="aavla2022">AAVLA 2022</a></li>
                                                            <!-- <li><a href="https://www.cronometrajeinstantaneo.com/inscripciones/aavla-2022" target="_blank">Inscribirte en AAVLA 2022</a></li> -->
                                                            <!-- <li><a href="https://cronometrajeinstantaneo.com/inscripciones/4-campus-de-entrenamiento-en-aguas-abiertas" target="_blank">Inscribirte en 4° Campus</a></li> -->
                                                            <li><a href="faq">Preguntas Frecuentes</a></li>
                                                            <li><a href="aavla2021">AAVLA 2021</a></li>
                                                            <li><a href="aavla2020">AAVLA 2020</a></li>
                                                            <li><a href="aavla2019">AAVLA 2019</a></li>
                                                            <li><a href="aavla2018">AAVLA 2018</a></li>
                                                            <li><a href="aavla2017">AAVLA 2017</a></li>
                                                            <li><a href="aavla2016">AAVLA 2016</a></li>
                                                            <li><a href="aavla2015">AAVLA 2015</a></li>
                                                            <li><a href="aavla2014">AAVLA 2014</a></li>
                                                            <li><a href="historia">Historia</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </nav>

                                    </div>
                                </header>

                                <div class="row">
                                    <div class="12u">
                                        <header class="first major">
                                            <br>
                                            <h2>Queremos que seas parte del grupo</h2>
                                            <span class="byline">Te invitamos a compartir fotos, experiencias y amistades en nuestras redes sociales</span>
                                            <a href="https://www.facebook.com/aguasabiertasvillalaangostura" target="_blank" class="button fa fa-facebook-square">Facebook</a>
                                            <a href="https://www.instagram.com/aguasabiertasvla" target="_blank" class="button fa fa-instagram">Instragram</a>
                                            <a href="mailto:aguasabiertasvla@gmail.com" target="_blank" class="button fa fa-envelope">Mail</a>
                                            <br><br>

<!-- Agregado del botón de Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like" data-href="https://www.facebook.com/aguasabiertasvillalaangostura" data-layout="standard" data-action="like" data-show-faces="true" data-share="true" style="padding-bottom: 30px;"></div>

<!-- Agregado del botón de Facebook -->
                                        </header>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Main Wrapper -->
            <div id="main-wrapper">

<?php if ($url != 'inicio'): ?>
                <div class="main-wrapper-style1" id="presentacion">
                    <div class="inner">

                        <!-- Feature 2 -->
                            <section class="container box-feature2">
                                <div class="row">
                                    <div class="12u">
                                        <section>

<?php include 'textos/'.$url.'.html'; ?>

                                        </section>
                                    </div>
                                </div>
                            </section>

                    </div>
                </div>

<?php else: ?>
                <div class="main-wrapper-style1" id="presentacion">
                    <div class="inner">

                        <!-- Feature 1 -->
                            <section class="container box-feature1">
                                <div class="row">
                                    <div class="12u">
                                    <h2>Somos una organización SIN FINES DE LUCRO Y SOLIDARIA</h2>
                                        <p>Estos son los 3 principios que nos llevan cada año a organizar eventos de aguas abiertas. Creemos en el deporte como un estilo de vida y transmitimos nuestra pasión mediante el ejemplo. Destinamos todos nuestros esfuerzos y todo lo que recaudamos, en causas que ayuden a cumplir estos principios.</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="4u">
                                        <section>
                                            <span class="image image-full"><img src="images/pic01.jpg" alt="" /></span>
                                            <header class="second fa fa-user">
                                                <h3>Disfrutar</h3>
                                                <span class="byline">TODOS de la natación</span>
                                            </header>
                                        </section>
                                    </div>
                                    <div class="4u">
                                        <section>
                                            <span class="image image-full"><img src="images/pic02.jpg" alt="" /></span>
                                            <header class="second fa fa-cog">
                                                <h3>Promocionar</h3>
                                                <span class="byline">este apasionante deporte</span>
                                            </header>
                                        </section>
                                    </div>
                                    <div class="4u">
                                        <section>
                                            <span class="image image-full"><img src="images/pic03.jpg" alt="" /></span>
                                            <header class="second fa fa-bar-chart-o">
                                                <h3>Aumentar</h3>
                                                <span class="byline">la Seguridad de la natación</span>
                                            </header>
                                        </section>
                                    </div>
                                </div>
                            </section>

                    </div>
                </div>

<?php include 'textos/'.$url.'.html'; ?>

<?php endif ?>

            </div>

        <!-- Sponsor Wrapper -->
            <div id="main-wrapper">
                <div class="main-wrapper-style1">
                    <div class="inner">
                        <div class="container">
                            <div class="row">
                                <div class="12u">

                                    <!-- Article list -->
                                        <section class="box-article-list">
                                            <h2>AGRADECEMOS A NUESTROS SPONSORS</h2>

<?php
foreach ($sponsors as $sponsor) {
    if ($sponsor['url'])
        echo '<a href="'.$sponsor['url'].'" target="_blank" class="image image-left" style="width: 250px; min-height: 250px;">';
    else
        echo '<span class="image image-left" style="width: 250px; min-height: 250px;">';
    if ($sponsor['logo'])
        echo '<img src="images/sponsors/'.$sponsor['logo'].'" alt="'.$sponsor['nombre'].'" />';
    else
        echo $sponsor['nombre'];
    if ($sponsor['url'])
        echo '</a>';
    else
        echo '</span>';
}
?>
                                        </section>
                                </div>
                            </div>
                            <div class="row">
                                <div class="12u">

                                        <section class="box-article-list">
                                            <h2>También agradecemos a los siguientes colaboradores</h2>

<?php
foreach ($colaboradores as $colaborador) {
    if ($colaborador['url'])
        echo '<a href="'.$colaborador['url'].'" target="_blank" class="image image-left" style="width: 150px; min-height: 150px;">';
    else
        echo '<span class="image image-left" style="width: 150px; min-height: 150px;">';
    if ($colaborador['logo'])
        echo '<img src="images/sponsors/'.$colaborador['logo'].'" alt="'.$colaborador['nombre'].'" />';
    else
        echo $colaborador['nombre'];
    if ($colaborador['url'])
        echo '</a>';
    else
        echo '</span>';
}
?>
                                        </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </body>
</html>